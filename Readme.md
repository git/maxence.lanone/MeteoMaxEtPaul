


<div align = center> <h1>La Météo de Paul et Maxence</h1></div>

Ce projet avait pour but de développer une application en React native permettant a un utilisateur de consulter la météo de son choix. Cette application devait utilisé une api, nous avons choisi d'utiliser [open-meteo](https://open-meteo.com) pour notre application.

<div align = center>

![](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)

&nbsp; ![React Native](https://img.shields.io/badge/react_native-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![Redux](https://img.shields.io/badge/redux-%23593d88.svg?style=for-the-badge&logo=redux&logoColor=white)
![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)
![Expo](https://img.shields.io/badge/expo-1C1E24?style=for-the-badge&logo=expo&logoColor=#D04A37)
![Android](https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=android&logoColor=white)
![iOS](https://img.shields.io/badge/iOS-000000?style=for-the-badge&logo=ios&logoColor=white)
![](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)

</div>

## :one: Maquetes

Vous pouvez accéder aux maquettes Figma de l'application depuis le lien suivant : [maquettes figma](https://www.figma.com/file/5BUjZQPLySqc9WUUYd8jS6/Toobo-M%C3%A9t%C3%A9o?node-id=0%3A1&t=5nDR5Asq7GXn9lFb-1)

## :two: API

Vous pouvez retrouver la documention de l'api depuis le lien suivant : [open-meteo.com](https://open-meteo.com/en/docs)



## :three: Avancement du projet

# :zap: Notation (Auto notation)

## Documentation (4 pts)

* :white_check_mark: Application sketches

### Basics (10 pts)

* :white_check_mark: Navigation (2 pts)
  * :white_check_mark: Tab bottom navigation + at least one button
* :white_check_mark: Store (2 pts)
  * :white_check_mark: Read data from redux store
* :white_check_mark: Actions (1 pts)
  * :white_check_mark: Update data to redux store
* :white_check_mark: Display list of items (2 pts)
  * :white_check_mark: FlatList, VirtualizedList or SectionList
* :white_check_mark: Display image (1 pts)
* :white_check_mark: Child props (1 pts)
* :white_check_mark: TextInput (1 pts)

### Application features (6 pts)

* :white_check_mark: Retrieve data using the Web API (2 pts)
* :construction: Store favorite data into phone storage (2 pts) `Note: Avait été mis en place par Paul mais il a oublié de push`
* :white_check_mark: Write Tests (2 pts)

### Bonus (only taken into account if the basics are all mastered)

* :x: Dark/Light mode switch (2pts) `Note: On sait le faire mais pas en utilisant redux, seulement avec useTheme`
* :construction: Sexy UI (2 pts) `Note: A vous de juger`

---

[Slides](https://iutsa01.blob.core.windows.net/react-native/ReactNative.pdf)
